$(document).ready(function () { 
  var $todoText = $('#todoText');
  var $addButton = $('#todoAdd');
  var $deleteButton = $('#todoDelete'); 
  var $moveUpButton = $('#todoMoveUp'); 
  var $moveDownButton = $('#todoMoveDown'); 
  var $editButton = $('#todoEdit');
  
  var todoList = new ItemList($('#myItemList'));
  var storageKey = 'CH-todo';
  var editing = false;
  
  function Item(title, element) {
    this.title = title;
    this.element = element;
    this.isMarked = false;
    this.isEditing = false;
    this.element.on('click', this.onClick.bind(this));
  }
  
  Item.prototype.render = function() {
      this.element.text(this.title);
      (this.isMarked) ? this.element.addClass('marked') : this.element.removeClass('marked');
  };
    
  Item.prototype.onClick = function() {
    if (this.isEditing) return;
    this.isMarked = !this.isMarked;
    this.render();
  };

  function ItemList(element) {
    this.element = element;
    this.items = [];
  }
  
  ItemList.prototype.render = function() {
    for (var item of this.items) {
      item.element.insertAfter(this.element.children().last());
      item.render();
    }
  };
    
  ItemList.prototype.add = function(title) {
    var itemElement = $(document.createElement('div'));
    this.items.push(new Item(title, itemElement));
    this.element.append(itemElement);
    this.element.on('click', this.onClick.bind(this));
  };
  
  ItemList.prototype.onClick = function() {
    $editButton.prop('disabled', (todoList.marked().length === 1) ? false : true);
  };
  
  ItemList.prototype.remove = function() {
    for (var i = this.items.length-1; i >= 0; i--) {
      var item = this.items[i];
      if (item.isMarked) {
        this.items.splice(i, 1);
        item.element.remove();
      }
    }
  };
  
  ItemList.prototype.moveUp = function() {
    for (var i = 0, len = this.items.length; i < len; i++) {
      var item = this.items[i];
      if (item.isMarked && i-1 >= 0 ) {
        this.items[i] = this.items[i-1];
        this.items[i-1] = item;
      }
    }
  };
  
  ItemList.prototype.moveDown = function() {
    for (var i = this.items.length-1; i >= 0; i--) {
      var item = this.items[i];
      if (item.isMarked && i+1 < this.items.length) {
        this.items[i] = this.items[i+1];
        this.items[i+1] = item;
      }
    }
  };
  
  ItemList.prototype.marked = function() {
    var marked = [];
    for (var item of this.items) {
      if (item.isMarked) {
        marked.push(item);
      }
    }
    return marked;
  };
  
  ItemList.prototype.editing = function(editing) {
    for (var item of this.items) {
      item.isEditing = editing;
    }
  };
  
  ItemList.prototype.loadFromStorage = function(key) {
    var storage = localStorage.getItem(key);
    if (storage !== null) {
      storage = JSON.parse(storage);
      for (var title of storage) {
        this.add(title);
      }
    }
  };
  
  ItemList.prototype.saveToStorage = function(key) {
    var toStore = [];
    for (var item of this.items) {
      toStore.push(item.title);
    }
    localStorage.setItem(key, JSON.stringify(toStore));
  };
  
  function disableButtons(isDisabled) {
    $addButton.prop('disabled', isDisabled);
    $deleteButton.prop('disabled', isDisabled); 
    $moveUpButton.prop('disabled', isDisabled);
    $moveDownButton.prop('disabled', isDisabled);
  }
  
  $addButton.on('click', function() {
    if ($todoText.val() === '') return;
    todoList.add($todoText.val());
    $todoText.val('');
    todoList.render();
    todoList.saveToStorage(storageKey);
  });
  
  $deleteButton.on('click', function() {
    todoList.remove();
    todoList.saveToStorage(storageKey);
  });
  
  $moveUpButton.on('click', function() {
    todoList.moveUp();
    todoList.render();
    todoList.saveToStorage(storageKey);
  });
  
  $moveDownButton.on('click', function() {
    todoList.moveDown();
    todoList.render();
    todoList.saveToStorage(storageKey);
  });
  
  $editButton.on('click', function() {
    editing = !editing;
    todoList.editing(editing);
    disableButtons(editing);
    if (editing) {
      $(this).text('Save');
      $todoText.val(todoList.marked()[0].title);
    } else {
      $(this).text('Edit');
      todoList.marked()[0].title = $todoText.val();
      $todoText.val('');
      todoList.render();
      todoList.saveToStorage(storageKey);
    }
  });
  
  todoList.loadFromStorage(storageKey);
  todoList.render();
});
